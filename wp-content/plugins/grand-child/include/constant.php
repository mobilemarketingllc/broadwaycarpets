<?php      
    if ( ! session_id() ) {
        session_start();
    }
    @ob_start();
    define('BASEURL','https://clients.mm-api.agency/');
    define('BLOGURL','https://blogs.mm-api.agency/');
    define('AUTHURL','oauth/token');
    define('SOCIALURL','credential/group/social');
    define('CONTACTURL','contact/');
    define('PRODUCTURL','www/catalog/');
    $cde = (get_option('CDE_ENV')) && get_option('CDE_ENV')!="" ?get_option('CDE_ENV'):""; 
    define('ENV', $cde);
    define('GOOGLE_MAP_KEY','AIzaSyD09zQ9PNDNNy9TadMuzRV_UsPUoWKntt8');
    //define('LAYOUT_COL','4');
    $LAYOUT_COL = get_option('layoutopotion')?get_option('layoutopotion'):0;    
 
    define('SOURCEURL','https://sfn.mm-api.agency/');
    define('AUTH_BASE_URL',"https://sso.mm-api.agency/");
    define('PROMOSAPI','http://promos.mm-api.agency/promos/current/');
    update_option('CLIENT_CODE','wordpress@canwilltech.com');
    update_option('CLIENTSECRET','NYESqMvXRHCpIsWHpHGkTnWm');
    update_option('gmt_offset','-5');
    
    if(get_option('getcouponbtn') ==''){ update_option('getcouponbtn','1'); }

    define('SFN_STATUS_PARAMETER', 'status=active&status=inactive&status=pending&status=dropped&status=deleted&status=remove&status=gone&status=unknown&allowZeroPrice=true&hideMissingImages=true');

    define('CLOUD_BASEURL','https://res.cloudinary.com/mobilemarketing/image/upload/');

    define('CLOUD_IMAGE_222',CLOUD_BASEURL.'c_limit,h_222,q_auto');
    define('CLOUD_IMAGE_600',CLOUD_BASEURL.'c_limit,h_600,w_400,q_auto'); 
    define('CLOUD_IMAGE_1000',CLOUD_BASEURL.'c_limit,h_1000,w_1600,q_auto');